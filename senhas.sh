#!/bin/bash

gerar_senha() {
	local tamanho=$1
	local caracteres_permitidos=$2
	local senha=""
	local indice_maximo=$(( ${#caracteres_permitidos} - 1 ))

	for ((i = 0; i < tamanho; i++)); do
    		local indice_aleatorio=$((RANDOM % indice_maximo))
    		senha+="${caracteres_permitidos:indice_aleatorio:1}"
	done

	echo "$senha"

}

opcoes=$(yad --form \
	--title "Gerador de Senhas" \
	--text "Escolha as opções para gerar a senha" \
	--field "Tamanho da Senha" \
	--field "Letras Maiúsculas:CHK" \
	--field "Letras Minúsculas:CHK" \
	--field "Números:CHK" \
	--field "Caracteres Especiais:CHK"
)

tamanho_senha=$(echo "$opcoes" | cut -d"|" -f1)
usar_maiusculas=$(echo "$opcoes" | cut -d"|" -f2)
usar_minusculas=$(echo "$opcoes" | cut -d"|" -f3)
usar_numeros=$(echo "$opcoes" | cut -d"|" -f4)
usar_caracteres_especiais=$(echo "$opcoes" | cut -d"|" -f5)

caracteres_permitidos=""
if [ "$usar_maiusculas" = "TRUE" ]; then
	caracteres_permitidos+="ABCDEFGHIJKLMNOPQRSTUVWXYZ"
fi

if [ "$usar_minusculas" = "TRUE" ]; then
	caracteres_permitidos+="abcdefghijklmnopqrstuvwxyz"
fi

if [ "$usar_numeros" = "TRUE" ]; then
	caracteres_permitidos+="0123456789"
fi

if [ "$usar_caracteres_especiais" = "TRUE" ]; then
	caracteres_permitidos+="!@#$%^&*()"
fi

if [ -z "$caracteres_permitidos" ]; then
	yad --error \
		--title "Erro" \
		--text "Selecione pelo menos uma opção de caracteres." 
	exit 1
fi

senha1=$(gerar_senha "$tamanho_senha" "$caracteres_permitidos" | sed 's/&/&amp;/g')
senha2=$(gerar_senha "$tamanho_senha" "$caracteres_permitidos" | sed 's/&/&amp;/g')
senha3=$(gerar_senha "$tamanho_senha" "$caracteres_permitidos" | sed 's/&/&amp;/g')

# Embaralhar as senhas geradas
senhas_embaralhadas=$(printf "%s\n%s\n%s\n" "$senha1" "$senha2" "$senha3" | shuf)

# Selecionar as três primeiras linhas (senhas)
senhas_selecionadas=$(echo "$senhas_embaralhadas" | head -n 3)

# Filtrar senhas vazias
senhas_filtradas=$(echo "$senhas_selecionadas" | grep -v '^$')

yad --info \
	--title "Senhas Geradas" \
	--text "$(echo "$senhas_filtradas" | sed 's/\n/\\n/g')" 

